@extends('layouts.app')
@section('title','Home')
@section('content')
<div class="row">
    <div class="col-md-10 offset-md-1">
        <form class="mt-4" action="{{ route('search.call') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col">
                   
                    <select class="form-control" id="user" name="user">
                        <option selected disabled>-- Please select user --</option>
                        @foreach ($uniqueUsers as $user)
                            <option value="{{ $user->user }}">{{ $user->user }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    
                    <select class="form-control" id="client" name="client">
                        <option selected disabled>-- Please select client --</option>
                        @foreach ($uniqueClients as $client)
                            <option value="{{ $client->client }}">{{ $client->client }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                   
                    <select class="form-control" id="type_of_call" name="type_of_call">
                        <option selected disabled>-- Please select type of call --</option>
                        <option value="Incoming">Incoming</option>
                        <option value="Outcoming">Outcoming</option>
                    </select>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary">Find</button>  
                    <a href="{{ route('welcome') }}" class="btn btn-primary" role="button">Select All</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row pt-5">
    <div class="col-md-10 offset-md-1">
        <h2 class="mb-2 text-center">List of all valid calls</h2>
        <table class="table table-hover table-bordered ">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Client</th>
                <th scope="col">Client Type</th>
                <th scope="col">Date</th>
                <th scope="col">Duration</th>
                <th scope="col">Type Of Call</th>
                <th scope="col">External Call Score</th>
                <th scope="row">Action</th>
                
              </tr>
            </thead>
            <tbody>
                @foreach ($users as $user )
                    <tr>
                        <th>{{ $user->id }}</th>
                        <th>{{ $user->user }}</th>
                        <th>{{ $user->client }}</th>
                        <th>{{ $user->client_type }}</th>
                        <th>{{ $user->created_at }}</th>
                        <th>{{ $user->duration }}</th>
                        <th>{{ $user->type_of_call }}</th>
                        <th>{{ $user->external_call_score }}</th>
                        <th>
                            <a href="{{ route('edit.call',$user->id) }}" class="btn btn-primary p-2" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="{{ route('delete.call',$user->id) }}" class="btn btn-danger p-2" role="button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </th>
                    </tr>
                @endforeach
             
            </tbody>
        </table>
    </div>
</div>

@endsection