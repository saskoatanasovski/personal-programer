@extends('layouts.app')
@section('title','User Data')
@section('content')

    <div class="row">
        <div class="col-md-8 offset-md-2 pb-5">
            <a href="{{ route('users') }}" class="btn btn-primary p-2 my-4 pull-right" role="button"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to users </a>
            <h2 class="mt-4">User name: {{ $userData[0]->user }}</h2>
            <h2 class="mb-4">Average user score: {{ $average }}</h2>

            <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Client</th>
                    <th scope="col">Client Type</th>
                    <th scope="col">Date</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Type Of Call</th>
                    <th scope="col">External Call Score</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
                    @foreach ($userData as $user )
                        <tr>
                            <th>{{ $user->id }}</th>
                            <th>{{ $user->user }}</th>
                            <th>{{ $user->client }}</th>
                            <th>{{ $user->client_type }}</th>
                            <th>{{ $user->created_at }}</th>
                            <th>{{ $user->duration }}</th>
                            <th>{{ $user->type_of_call }}</th>
                            <th>{{ $user->external_call_score }}</th>
                           
                        </tr>
                    @endforeach
                 
                </tbody>
            </table>
            
        </div>
    </div>
@endsection