@extends('layouts.app')
@section('title','Users')
@section('content')

<div class="row">
    <div class="col-md-6 offset-md-3 mt-5">
        <h2 class="mb-2 text-center">Users</h2>
        <table class="table table-hover table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">User</th>
                    <th scope="row">Action</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user )
                <tr>
                    <th>{{ $user->user }}</th>
                    <th>
                        <a href="{{ route('select.user.view',$user->user) }}" class="btn btn-primary p-2" role="button">View</a>
                    </th>
                </tr>
                @endforeach

            </tbody>
        </table>
        
        
       
</div>
@endsection
