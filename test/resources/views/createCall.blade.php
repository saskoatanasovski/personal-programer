@extends('layouts.app')
@section('title','Create Call')
@section('content')
<div class="row ">
    <div class="col-md-8 offset-md-2 mt-5">
        <h2 class="mb-2 text-center">Create call</h2>
        <form action="{{ route('store.call') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="user">User</label>
                <input type="text" class="form-control" id="user" name="user" placeholder="ex John Doe" value="{{ old('user') }}">
                @error('user')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="client">Client</label>
                <input type="text" class="form-control" id="client" name="client" placeholder="ex Jane Doe" value="{{ old('client') }}">
                @error('client')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="client_type">Client Type</label>
                <input type="text" class="form-control" id="client_type" name="client_type" placeholder="ex nurse" value="{{ old('client_type') }}">
                @error('client_type')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="duration">Duration</label>
                <input type="number" class="form-control" id="duration" name="duration" placeholder="minutes" value="{{ old('duration') }}">
                @error('duration')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="type_of_call">Type Of Call</label>
                <select class="form-control" id="type_of_call" name="type_of_call">
                    <option selected disabled>-- Please select type of call --</option>
                    <option value="Incoming">Incoming</option>
                    <option value="Outcoming">Outcoming</option>
                </select>
                @error('type_of_call')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="external_call_score">External Call Score </label>
                <input type="number" class="form-control" id="external_call_score" name="external_call_score" value="{{ old('external_call_score') }}">
            </div>
            @error('external_call_score')
                <div class="text-danger mt-2">{{ $message }}</div>
            @enderror
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</div>
@endsection
