@extends('layouts.app')
@section('title','IMport File')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3 mt-5">
            <h2 class="mb-2 text-center">Import CSV file</h2>

            <form action="{{ route('storeFile') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="form-group">
                    <label for="importFile">Import File</label>
                    <input type="file" class="form-control p-3 h-25" id="importFile" name="importFile"  value="{{ old('importFile') }}">
                    @error('user')
                        <div class="text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary">Import</button>
            </form>

            @if(Session::has('msg'))
            <div class="alert  alert-danger mt-4">
                
                {{  Session::get('msg') }}
            </div>
        @endif
        </div>
    </div>
@endsection