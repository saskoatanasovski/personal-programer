<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
class welcomeController extends Controller
{
  public function welcome(){
    $uniqueUsers = User::select('user')->distinct()->get();
    $uniqueClients = User::select('client')->distinct()->get();
    $users = User::where('duration','>', 10)->get();

    return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);

  }

  public function searchCall(Request $req){
    
    $uniqueUsers = User::select('user')->distinct()->get();
    $uniqueClients = User::select('client')->distinct()->get();

    if ($req->input('client')) {
      $users = User::where('client',$req->input('client'))->get();
      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }

    if($req->input('user')){
      $users = User::where('user',$req->input('user'))->get();
      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }
    
    if($req->input('type_of_call')){
      $users = User::where('type_of_call',$req->input('type_of_call'))->get();
      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }

    if($req->input('client') && $req->input('user')){
      $users = User::where('client',$req->input('client'))
                  ->whereNotNull('user',$req->input('user'))
                  ->get();

      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }

    if($req->input('client') && $req->input('type_of_call')){
      $users = User::where('client',$req->input('client'))
                    ->whereNotNull('type_of_call',$req->input('type_of_call'))
                    ->get();

      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }

    if($req->input('user') && $req->input('type_of_call')){
      $users = User::where('user',$req->input('user'))
                    ->whereNotNull('type_of_call',$req->input('type_of_call'))
                    ->get();

      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }

    if($req->input('user') && $req->input('type_of_call') && $req->input('client')){
      $users = User::where('user',$req->input('user'))
                    ->whereNotNull('type_of_call',$req->input('type_of_call'))
                    ->whereNotNull('client',$req->input('client'))
                    ->get();

      return view('welcome',['users'=>$users,'uniqueUsers'=>$uniqueUsers,'uniqueClients'=>$uniqueClients]);
    }
  }

  public function  getAllUsers(){

    $users = User::select('user')->distinct()->get();

    return view('users',['users'=>$users]);
  }

  public function importFile(){
    return view('importFile');
  }

  public function storeFile(Request $req){
    
    $req->validate([
      'ipmortFIle'=>'file|filled'
    ]);
    
    $file = $req->file('importFile');
    $newFile = $file->getClientOriginalName();
    $destination = public_path(('/CSVFile'));
    if (File::exists(public_path("/CSVFile/{$newFile}"))) {
      return redirect()->route('importFile')->with('msg',"The file '{$newFile}' already exist please import different file");
    }
    else{
      $file->move($destination,$newFile);
    }
    $fileToImport = File::get(public_path("/CSVFile/{$newFile}"));

    $rawData = explode("\n",$fileToImport);
    
   
    foreach ($rawData as $key => $data) {
      if ($key == '0') {
        continue;
      }
      else{
        $dataToBase = explode(',',$data);
        
        User::create([
          'user'=>$dataToBase[0],
          'client'=>$dataToBase[1],
          'client_type'=>$dataToBase[2],
          'CSVdate'=>$dataToBase[3],
          'duration'=>$dataToBase[4],
          'type_of_call'=>$dataToBase[5],
          'external_call_score'=>$dataToBase[6]
        ]);
      }
    }
    return redirect()->route('welcome');
  }

  public function selectUserView($input){
      

      $users = User::where('user',$input)->limit(5)->get();
      $average = $users->avg('external_call_score');
      
      return view('userData',['userData'=>$users,'average'=>round($average,2)]);
  }
}
