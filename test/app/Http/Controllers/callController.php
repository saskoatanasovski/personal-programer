<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class callController extends Controller
{
    public function create(){
        return view('createCall');
    }

    public function store(Request $req){
        $req->validate([
            'user'=>'required|string',
            'client'=>'required|string',
            'client_type'=>'required|string',
            'duration'=>'required|numeric',
            'type_of_call'=>'required',
            'external_call_score'=>'required|numeric'
        ]);

        User::create([
            'user'=>$req->input('user'),
            'client'=>$req->input('client'),
            'client_type'=>$req->input('client_type'),
            'duration'=>$req->input('duration'),
            'type_of_call'=>$req->input('type_of_call'),
            'external_call_score'=>$req->input('external_call_score')
        ]);
        return redirect()->route('welcome');
    }

    public function edit($id){

        $user = User::where('id',$id)->first();

        return view('editCall',['user'=>$user]);

    }

    public function update(Request $req,$id){
        $req->validate([
            'user'=>'required|string',
            'client'=>'required|string',
            'client_type'=>'required|string',
            'duration'=>'required|numeric',
            'type_of_call'=>'required',
            'external_call_score'=>'required|numeric'
        ]);

        User::where('id',$id)->update([
            'user'=>$req->input('user'),
            'client'=>$req->input('client'),
            'client_type'=>$req->input('client_type'),
            'duration'=>$req->input('duration'),
            'type_of_call'=>$req->input('type_of_call'),
            'external_call_score'=>$req->input('external_call_score')
        ]);

        return redirect()->route('welcome');
    }

    public function delete($id){
        $call = User::where('id',$id)->first();
        $call->delete();

        return redirect()->route('welcome');
    }

    
}