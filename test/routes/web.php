<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[App\Http\Controllers\welcomeController::class, 'welcome'])->name('welcome');
Route::get('/users',[App\Http\Controllers\welcomeController::class, 'getAllUsers'])->name('users');
Route::get('/importfile',[App\Http\Controllers\welcomeController::class, 'importFile'])->name('importFile');
Route::post('/storefile',[App\Http\Controllers\welcomeController::class, 'storeFile'])->name('storeFile');
Route::get('/userdata{input}',[App\Http\Controllers\welcomeController::class, 'selectUserView'])->name('select.user.view');
Route::post('/searchcall',[App\Http\Controllers\welcomeController::class, 'searchCall'])->name('search.call');
Route::get('/createcall',[App\Http\Controllers\callController::class, 'create'])->name('create.call');
Route::post('/storecall',[App\Http\Controllers\callController::class, 'store'])->name('store.call');
Route::get('/editcall{id}',[App\Http\Controllers\callController::class, 'edit'])->name('edit.call');
Route::post('/updatecall{id}',[App\Http\Controllers\callController::class, 'update'])->name('update.call');
Route::get('/deletecall{id}',[App\Http\Controllers\callController::class, 'delete'])->name('delete.call');


